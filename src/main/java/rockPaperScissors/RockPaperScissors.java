package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    public void run() {
    	
    	while (true) {
    		String humanChoice = userChoice();
    		
    		if ((!(humanChoice.equals("rock")) && (!(humanChoice.equals("paper")) && (!(humanChoice.equals("scissors")))))) {
    			System.out.println("I do not understand " + humanChoice + ". Could you try again?");
    		}
    		else {
    			roundCounter += 1;
    			String computerHand = computerChoice();
    			
    			boolean resultGame = userWins(humanChoice, computerHand);
    			
    			if (resultGame) {
    				humanScore += 1;
    			}
    			else if (humanChoice.equals(computerHand)) {
    			}
    			else {
    				computerScore += 1;
    			}
    			
    			String printWinner = whoWins(resultGame, humanChoice, computerHand);
    			System.out.println("Human chose " + humanChoice + ", computer chose " + computerHand + ". " + printWinner);
    			System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    			String playAgain = readInput("Do you wish to continue playing? (y/n)?");
    			
    			if (playAgain.equals("n")) {
    				System.out.println("Bye bye :)");
    				break;
    			}
    			else if (!(playAgain.equals("y"))) {
    				System.out.println("I do not understand " + playAgain + ". Could you try again?");
    			}
    		}
    	}	
    }

    
    
    public String userChoice() {
    	
    	System.out.println("Let's play round " + roundCounter);
    	String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
    	humanChoice = humanChoice.toLowerCase();
    	
    	return humanChoice;
    	}
    
    
    public String computerChoice() {
    	
    	int random = (int) (Math.random() * 3);
    	
    	if (random == 0) {
    		return "rock";
    	}
    	else if (random == 1) {
    			return "paper";
    	}
    	else {
    		return "scissors";
    	}
    }
    
    
    public boolean userWins (String userHand, String computerHand) {
    	
    	if (userHand.equals("rock")) {
    		return computerHand.equals("scissors");
    	}
    	else if (userHand.equals("paper")) {
    		return computerHand.equals("rock");
    	}
    	else {
    		return computerHand.equals("paper");
    	}
    }
    
    
    public String whoWins(boolean resultGame, String humanChoice, String computerHand) {

    	if (humanChoice.equals(computerHand)) {
    		return "it´s a tie!";
    	}
    	else if (resultGame) {
    		return "Human wins!";
    	}
    	else {
    		return "Computer wins!";
    	}    	
    }
    
  
    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}
